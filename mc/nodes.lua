minetest.register_node("mc:warped_fungus", {
    drawtype = "plantlike",
    tiles = {"warped_fungus.png"}
})
minetest.register_node("mc:sweet_berry_bush_0", {
    drawtype = "plantlike",
    tiles = {"sweet_berry_bush_0.png"},
    damage_per_second = 1
})
minetest.register_node("mc:sweet_berry_bush_1", {
    drawtype = "plantlike",
    tiles = {"sweet_berry_bush_1.png"},
    damage_per_second = 1
})
minetest.register_node("mc:sweet_berry_bush_2", {
    drawtype = "plantlike",
    tiles = {"sweet_berry_bush_2.png"},
    damage_per_second = 2,
    drop = "mc:sweet_berry 2"
})
minetest.register_node("mc:sweet_berry_bush_3", {
    drawtype = "plantlike",
    tiles = {"sweet_berry_bush_3.png"},
    damage_per_second = 2,
    drop = "mc:sweet_berry 3"
})