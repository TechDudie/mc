minetest.register_decoration({
    deco_type = "simple",
    place_on = {"mcl_nether:netherrack"},
    sidelen = 16,
    fill_ratio = 0.05,
    biomes = {"Nether"},
    y_max = mcl_vars.mg_nether_max,
    y_min = mcl_vars.mg_nether_min,
    decoration = "mc:warped_fungus" -- TODO: Add Warped and Crimson Forest and place fungus in correct biomes
})
minetest.register_decoration({
     deco_type = "simple",
     place_on = {"mcl_core:dirt_with_grass"},
     sidelen = 16,
     fill_ratio = 0.05,
     biomes = {"Taiga","Forest"},
     y_max = mcl_vars.mg_overworld_max,
     y_min = mcl_vars.mg_overworld_min,
     decoration = "mc:sweet_berry_bush_2"
 })