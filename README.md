# mc
mc is a minetest modpack for the game MineClone2. The goal is to turn it into Minecraft 1.16.

# Credits

* Nether gold ore (NO11)
* Extra Mobs (epCode)
* Bubble Columns (Minetest-j45)
* Nether Mushrooms (debiankaios)
* Blackstone (debian044)

# Done:

* Nether gold ore (NO11)
* Extra Mobs (epCode)
* Bubble Columns (Minetest-j45)
* Nether Mushrooms (debiankaios)
* Sweet Berries
* Fungus on a Stick

# In Progess:

* Bamboo
* Campfire
* Honey
* Netherite
* Scaffolding
* Tridents

# Code, Textures and Models

I often need code, textures, and models to complete things. I greatly appreciate these contributions.

## Code

I'm not *too* picky about code, but if the code is extremly messy, i will clean it up.

### Code needed:

Tridents: mc_tridents

## Textures

I'd like for the textures to be as close to Minecraft's textures as possible.

### Textures needed:

Tridents: mc_tridents
Ancient Debris: mc_netherite
Netherite Scrap: mc_netherite
Block of Netherite: mc_netherite (improvement)
Bamboo (all stages of growth and inventory image): mc_bamboo
Scaffolding: mc_scaffolding

## Models

I dont plan on making the models for mobs myself, epCode is doing it in his repo extra_mobs on https://git.minetest.land/epCode/extra_mobs, but I do include entities.

### Models needed:

Tridents: mc_tridents

# TODO List:

* Sweet Berries and Foxes
* New structures
* Nether Biomes
* ...
